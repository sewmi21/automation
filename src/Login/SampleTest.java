package Login;

import Reporting.CustomAnnotations;
import Reporting.CustomReporter;
import Reporting.MyTestResultListener;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.AfterTest;
import org.testng.annotations.CustomAttribute;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import org.yaml.snakeyaml.scanner.Constant;
import utils.Constants;

@Listeners(MyTestResultListener.class)
public class SampleTest {
    private CustomAttribute id;
    private CustomAttribute name;

    @Test()
        public void testMethodOne(ITestContext context) {
            context.setAttribute("testid",1);
            Assert.assertTrue(true);
        }
         @Test(dependsOnMethods = {"testMethodOne"})
        public void testMethodTwo(ITestContext context) {
            context.setAttribute("testid",2);
            Assert.assertTrue(true);
        }
        @Test(dependsOnMethods = {"testMethodTwo"})
        public void testMethodThree(ITestContext context) {
            context.setAttribute("testid",3);
            Assert.assertTrue(false);
        }
        @Test(dependsOnMethods = {"testMethodThree"})
        public void testMethodFour(ITestContext context) {
            context.setAttribute("testid",4);
            Assert.assertTrue(true);
        }
        @Test(dependsOnMethods = {"testMethodFour"})
        public void testMethodFive(ITestContext context) {
           context.setAttribute("testid",5);
           Assert.assertTrue(true);
         }
}
