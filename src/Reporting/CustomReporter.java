package Reporting;

import org.testng.*;
import org.testng.xml.XmlSuite;

import java.util.*;

public class CustomReporter implements IReporter {
    @Override
//    public void generateReport(List<XmlSuite> xmlSuites, List<ISuite> suites, String outputDirectory,ITestResult result) {
//
//        for (ISuite suite : suites) {
//            System.out.println("Suite name - " + suite.getName());
//            Map<String, ISuiteResult> suiteResults = suite.getResults();
//            for (ISuiteResult sr : suiteResults.values()) {
////                ITestContext context= result.getTestContext();
////                context.getAttribute("testid");
//                ITestContext tc = sr.getTestContext();
//                System.out.println("Test tag name: " + tc.getName() +
//                        "Test start time: " + tc.getStartDate() +
//                        "Test end time: " + tc.getEndDate() +
//                        "Test report output dir: " + tc.getOutputDirectory());
//                Collection<ITestNGMethod> failedMethods = tc.getFailedTests().getAllMethods();
//                Collection<ITestNGMethod> passedMethods = tc.getPassedTests().getAllMethods();
//                Collection<ITestNGMethod> skippedMethods = tc.getSkippedTests().getAllMethods();
//                System.out.println("Total failed methods: "+ failedMethods.size() +" and those are: ");
//                for(ITestNGMethod itm : failedMethods){
//                    System.out.println("Failed test methods Name: " + itm.getMethodName());
//                }
//                for(ITestNGMethod itm : passedMethods){
//                    System.out.println("Passed test Method Name: " + itm.getMethodName());
//                }
//                for(ITestNGMethod itm : skippedMethods){
//                    System.out.println("skipped test Method Name: " + itm.getMethodName());
//                }
//
//                System.out.println("Total tests for suite is :" + tc.getAllTestMethods().length);
//                System.out.println("Passed tests for suite is :" + tc.getPassedTests().getAllResults().size());
//                System.out.println("Failed tests for suite is :" + tc.getFailedTests().getAllResults().size());
//                System.out.println("Skipped tests for suite is:" + tc.getSkippedTests().getAllResults().size());
//
//
//
//            }
//        }
//        System.out.println("Path - " + outputDirectory);

//    }
    public void generateReport(List<XmlSuite> xmlSuites, List<ISuite> suites,
                               String outputDirectory) {

        //Iterating over each suite included in the test
        for (ISuite suite : suites) {
            //Following code gets the suite name
            String suiteName = suite.getName();
            //Getting the results for the said suite
            Map<String, ISuiteResult> suiteResults = suite.getResults();
            for (ISuiteResult sr : suiteResults.values()) {
                ITestContext tc = sr.getTestContext();

                System.out.println("Test tag name: " + tc.getName() +
                        "Test start time: " + tc.getStartDate() +
                        "Test end time: " + tc.getEndDate() +
                        "Test report output dir: " + tc.getOutputDirectory());

                Collection<ITestNGMethod> failedMethods = tc.getFailedTests().getAllMethods();
                Collection<ITestNGMethod> skippedMethods = tc.getSkippedTests().getAllMethods();
                Collection<ITestNGMethod> passedMethods = tc.getPassedTests().getAllMethods();

                Map<String,String> testMap = new HashMap<>();
                for(ITestNGMethod itm : failedMethods){
                    System.out.println("Passed test Method Name: " + itm.getMethodName());
                    System.out.println("Passed test methods Name: " + tc.getAttribute("testid"));
                    testMap.put(itm.getMethodName(),"Failed");
                }
                for(ITestNGMethod itm : passedMethods){
                    System.out.println("Passed test Method Name: " + itm.getMethodName());
                    System.out.println("Passed test methods Name: " + tc.getAttribute("testid"));
                    testMap.put( itm.getMethodName(),"Passed");
                }
                for(ITestNGMethod itm : skippedMethods){
                    System.out.println("skipped test Method Name: " + itm.getMethodName());
                    System.out.println("skipped test methods Name: " + tc.getAttribute("testid"));
                    testMap.put( itm.getMethodName(),"Skipped");
                }
                //pass test method results
                for (Map.Entry<String, String> set : testMap.entrySet()) {
                    System.out.println(set.getKey() + " = " + set.getValue());
                }
                //Total number of tests
                Integer totalNumOfTests=tc.getAllTestMethods().length;
                //Total Number of Passed Tests
                Integer totalNumOfPassedTests=tc.getPassedTests().getAllResults().size();
                //Total Number of Failed Tests
                Integer totalNumOfFailedTests=tc.getFailedTests().getAllResults().size();
                //Total Number of Skipped Tests
                Integer totalNumOfSkippedTests=tc.getSkippedTests().getAllResults().size();

            }
        }
    }
}
