package Reporting;

import org.testng.IClass;
import org.testng.ITestListener;
import org.testng.ITestResult;
import org.testng.TestListenerAdapter;

import java.lang.reflect.Method;

public class MyTestResultListener implements ITestListener {

    @Override
    public void onTestFailure(ITestResult result){
        System.out.println("Failed test cases"+result.getTestName());
    }
    @Override
    public void onTestSuccess(ITestResult arg0){

    }
    @Override
    public void onTestSkipped(ITestResult arg0){

    }
}
